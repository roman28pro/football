package ru.kra.football;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private Integer click = 0;
    private Integer click2 = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clicker2(View view) {
        click2++;
        TextView textView = (TextView) findViewById(R.id.textView4);
        textView.setText(click2.toString());
    }

    public void clicker(View view) {
        click++;
        TextView textView = (TextView) findViewById(R.id.textView5);
        textView.setText(click.toString());
    }

    public void sbros(View view) {
        click = 0;
        click2 = 0;
        TextView textView = (TextView) findViewById(R.id.textView4);
        TextView textView2 = (TextView) findViewById(R.id.textView5);
        textView.setText(click.toString());
        textView2.setText(click.toString());
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("click", click);
        outState.putInt("click2", click2);
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        click = savedInstanceState.getInt("click");
        click2 = savedInstanceState.getInt("click2");
    }
}